import cv2
import numpy as np
import dlib
from math import hypot
from collections import deque
from imutils.video import VideoStream
import argparse
import imutils
import time
import pyautogui
from pynput.mouse import Button , Controller
from PyQt5 import QtCore, QtWidgets, QtGui
from threading import Thread
import sys
import time

(camx,camy)=(640,480)

def frameToPixmap(frame):
    print('display')
    frame = cv2.cvtColor(np.uint8(frame), cv2.COLOR_BGR2RGB) 
    (height, width, channel) = frame.shape
    image = QtGui.QImage(frame, width, height, width*3, QtGui.QImage.Format_RGB888)
    return QtGui.QPixmap(image)

# [GUI]
qapp = QtWidgets.QApplication(sys.argv)
screenGeo = qapp.desktop().availableGeometry()
sx = screenGeo.width()
sy = screenGeo.height() # maximum screen size

print((sx, sy))

mainLabel = QtWidgets.QLabel()

min_x = (sx - camx)//2
max_x = (sx + camx)//2
min_y = (sy - camy)//2
max_y = (sx + camy)//2

mainLabel.move(min_x, min_y)
mainLabel.setFixedSize(camx, camy)
mainLabel.show()
print(mainLabel.size())

# [/GUI]

mouse = Controller()
mouse.position = (sx/2,sy/2)
pmouseloc = np.array([0,0]) #previous mouse location
mouseloc = np.array([0,0]) # mouse location
damp_value = 1



 # phai >1
#___________________________________________________________________
# construct the argument parse and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-b", "--buffer", type=int, default=32,
    help="max buffer size")
args = vars(ap.parse_args())
ap.add_argument("-v", "--video",
    help="path to the (optional) video file")
board = np.zeros((500,500),np.uint8)
board[:] = 255
#____________________________________________________________________
pts = deque(maxlen=args["buffer"])
counter = 0
(dX, dY) = (0, 0)
direction = ""

if not args.get("video", False):
    vs = VideoStream(src=0).start()
 
# otherwise, grab a reference to the video file
else:
    vs = cv2.VideoCapture(args["video"])
 
# allow the camera or video file to warm up
time.sleep(2.0)
#___________________________________________________________________
#cap = cv2.VideoCapture(0)
detector = dlib.get_frontal_face_detector()
predictor = dlib.shape_predictor("data/shape_predictor_68_face_landmarks.dat")
#eye_cascade =cv2.CascadeClassifier('data/haarcascade_eye.xml')
keyboard = np.zeros((450, 1000, 3), np.uint8)
keys_set_1 = {0: "Q", 1: "W", 2: "E", 3: "R", 4: "T",15:"Y",16:"U",17:"I",18:"O",19:"P",
              5: "A", 6: "S", 7: "D", 8: "F", 9: "G",20:"H",21:"J",22:"K",23:"L",
              10: "Z", 11: "X", 12: "C", 13: "V", 14: "B",24:"N",25:"M",26:"SPACE"}
#_______________________________________________________________________________________________________
def letter(letter_index, text, letter_light,style):
    # Keys
    if letter_index == 0:
        x = 0
        y = 0
    elif letter_index == 1:
        x = 100
        y = 0
    elif letter_index == 2:
        x = 200
        y = 0
    elif letter_index == 3:
        x = 300
        y = 0
    elif letter_index == 4:
        x = 400
        y = 0
    elif letter_index == 15:
        x = 500
        y = 0
    elif letter_index == 16:
        x = 600
        y = 0
    elif letter_index == 17:
        x = 700
        y = 0
    elif letter_index == 18:
        x = 800
        y = 0
    elif letter_index == 19:
        x = 900
        y = 0
    elif letter_index == 5:
        x = 50
        y = 100
    elif letter_index == 6:
        x = 150
        y = 100
    elif letter_index == 7:
        x = 250
        y = 100
    elif letter_index == 8:
        x = 350
        y = 100
    elif letter_index == 9:
        x = 450
        y = 100
    elif letter_index == 20:
        x = 550
        y = 100
    elif letter_index == 21:
        x = 650
        y = 100
    elif letter_index == 22:
        x = 750
        y = 100
    elif letter_index == 23:
        x = 850
        y = 100
    elif letter_index == 10:
        x = 150
        y = 200
    elif letter_index == 11:
        x = 250
        y = 200
    elif letter_index == 12:
        x = 350
        y = 200
    elif letter_index == 13:
        x = 450
        y = 200
    elif letter_index == 14:
        x = 550
        y = 200
    elif letter_index == 24:
        x = 650
        y = 200
    elif letter_index == 25:
        x = 750
        y = 200
    elif letter_index ==26:
        x = 250
        y = 300
    th = 3 # thicknes
    font_letter = cv2.FONT_HERSHEY_PLAIN
    font_scale = 5
    font_th = 3
    text_size = cv2.getTextSize(text, font_letter, font_scale, font_th)[0]
    width_text, height_text = text_size[0], text_size[1]
    if style is 1 :
        # Text settings
        width = 100
        height = 100
        text_x = int((width - width_text) / 2) + x
        text_y = int((height + height_text) / 2) + y
        cv2.putText(keyboard, text, (text_x, text_y), font_letter, font_scale, (0, 255, 0), font_th)
    else :
        width = 500
        height =100
        text_x = int((width - width_text) / 2) + x
        text_y = int((height + height_text) / 2) + y
        cv2.putText(keyboard, text, (text_x, text_y), font_letter, font_scale, (0, 255, 0), font_th)
    if letter_light is True:
        cv2.rectangle(keyboard, (x + th, y + th), (x + width - th, y + height - th), (255, 255, 255),6)
    else:
        cv2.rectangle(keyboard, (x + th, y + th), (x + width - th, y + height - th), (255, 0, 0), th)    
#_______________________________________________________________________________________________________
def midpoint(p1 ,p2):
    return int((p1.x + p2.x)/2), int((p1.y + p2.y)/2)
 
font = cv2.FONT_HERSHEY_PLAIN
#_______________________________________________________________________________________________________
def get_blinking_ratio(eye_points, facial_landmarks):
    left_point = (facial_landmarks.part(eye_points[0]).x, facial_landmarks.part(eye_points[0]).y)
    right_point = (facial_landmarks.part(eye_points[3]).x, facial_landmarks.part(eye_points[3]).y)
    center_top = midpoint(facial_landmarks.part(eye_points[1]), facial_landmarks.part(eye_points[2]))
    center_bottom = midpoint(facial_landmarks.part(eye_points[5]), facial_landmarks.part(eye_points[4]))
    mid3840=midpoint(facial_landmarks.part(eye_points[2]), facial_landmarks.part(eye_points[4]))
    mid3741=midpoint(facial_landmarks.part(eye_points[1]), facial_landmarks.part(eye_points[5]))
 
 
    #hor_line = cv2.line(frame, left_point, right_point, (0, 255, 0), 1)
    # ver_line = cv2.line(frame, center_top, center_bottom, (0, 255, 0), 1)
    # line1= cv2.line(frame, mid3840, mid3741, (255, 0, 0), 1)
 
    hor_line_lenght = hypot((left_point[0] - right_point[0]), (left_point[1] - right_point[1]))
    ver_line_lenght = hypot((center_top[0] - center_bottom[0]), (center_top[1] - center_bottom[1]))
 
    ratio = hor_line_lenght / ver_line_lenght
    
    return ratio
#_______________________________________________________________________________________________________ 
text= ""
exit_ = False
def mainProcessingLoop():
    global pmouseloc
    global mouseloc
    global text
    global exit_
    try:
        while True:
            #ret,orframe = cap.read()
            orframe = vs.read()
            frame1 = cv2.flip(orframe,1)
            frame = cv2.resize(frame1,(camx,camy))
            print('get frame')
            print(frame.shape)
            blurred_frame=cv2.GaussianBlur(frame, (5, 5), 0)
            gray_frame = cv2.cvtColor(blurred_frame,cv2.COLOR_BGR2GRAY)
            #_________________________________
            frame = frame[1] if args.get("video", False) else frame
            if frame is None:
                break
            #_________________________________

            faces = detector(gray_frame)
            for face in faces:
                landmarks = predictor(gray_frame, face)
                #Detect blinking
                left_eye_ratio = get_blinking_ratio([36, 37, 38, 39, 40, 41], landmarks)
                right_eye_ratio = get_blinking_ratio([42, 43, 44, 45, 46, 47], landmarks)
                blinking_ratio = (left_eye_ratio + right_eye_ratio) / 2
                #Detect eye gaze_________________________________________________________________
                Leye_region = np.array([ (landmarks.part(36).x,landmarks.part(36).y),
                                        (landmarks.part(37).x,landmarks.part(37).y),
                                        (landmarks.part(38).x,landmarks.part(38).y),
                                        (landmarks.part(39).x,landmarks.part(39).y),
                                        (landmarks.part(40).x,landmarks.part(40).y),
                                        (landmarks.part(41).x,landmarks.part(41).y)],np.int32)

                #_______ROI EYE_________________________________________________________________
                min_x = np.min (Leye_region[:,0])
                max_x = np.max (Leye_region[:,0])
                min_y = np.min (Leye_region[:,1])
                max_y = np.max (Leye_region[:,1])
                left_eye =  frame[min_y : max_y , min_x : max_x]
                blurred_eye=cv2.GaussianBlur(left_eye, (5, 5), 0)
                gray_eye = cv2.cvtColor(blurred_eye,cv2.COLOR_BGR2GRAY)
                #img = cv2.medianBlur(gray_eye,3)
                ret,th1 = cv2.threshold(gray_eye,70,255,cv2.THRESH_BINARY_INV)
                contours = cv2.findContours(th1, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
                #contour = sorted(contour, key=lambda x: cv2.contourArea(x), reverse=True)
                contours = imutils.grab_contours(contours)
                center = (0,0)
                #____________________________________________________________________
                if ((blinking_ratio < 6 ) and (len(contours) > 0)) :
                    # (ex,ey,ew,eh) = cv2.boundingRect(contours)
                    # cv2.rectangle(left_eye,(ex,ey),(ex+ew,ey+eh),(0,255,0),1)
                    c = max(contours, key=cv2.contourArea)
                    ((x, y), radius) = cv2.minEnclosingCircle(c)
                    M = cv2.moments(c)
                    if M["m00"] != 0:
                        cX = int(M["m10"] / M["m00"])
                        cY = int(M["m01"] / M["m00"])
                        center = (cX, cY)  #center
                    else:
                        center = (1,1)
                    cv2.circle(frame, center, 2, (0, 0, 255), 2)
                    cv2.circle(left_eye,(int(x),int(y)),int(radius),(0,255,255),1)
                    cv2.circle(left_eye, center, 2, (0, 0, 255), 2)
                    pts.appendleft(center)
                    mouse.release(Button.left)
                    mouseloc= pmouseloc + (center-pmouseloc)/damp_value  
                    # mouse.position = (10*mouseloc[0]*sx/camx,40*mouseloc[1]*sy/camy)
                    pmouseloc = mouseloc
                elif ((blinking_ratio > 6 ) and (len(contours) > 0)) :
                    c = max(contours, key=cv2.contourArea)
                    ((x, y), radius) = cv2.minEnclosingCircle(c)
                    M = cv2.moments(c)
                    if M["m00"] != 0:
                        cX = int(M["m10"] / M["m00"])
                        cY = int(M["m01"] / M["m00"])
                        center = (cX, cY)  #center
                    else:
                        center = (1,1)
                    cv2.circle(frame, center, 2, (0, 0, 255), 2)
                    cv2.circle(left_eye,(int(x),int(y)),int(radius),(0,255,255),1)
                    cv2.circle(left_eye, center, 2, (0, 0, 255), 2)
                    pts.appendleft(center)
                    mouseloc= pmouseloc + (center-pmouseloc)/damp_value  
                    # mouse.position = (10*mouseloc[0]*sx/camx,40*mouseloc[1]*sy/camy)
                    pmouseloc = mouseloc
                    c_G_letter = (sx/2,sy/2)
                    if (abs(10*mouseloc[0]*sx/camx-c_G_letter[0]) < 225) and (abs(40*mouseloc[1]*sy/camy-c_G_letter[1]) < 500) :  
                        cv2.putText(frame, "BLINKING", (50, 150), font, 7, (255, 0, 0))
                        mouse.press(Button.left)
                        activate_letter = keys_set_1[letter_index]
                        text+= activate_letter
                    else:
                        mouse.release(Button.left)



                #____________________________________________________________________
            # Letters
            for i in range(27):
                if i == 9:
                    light = True
                    letter(i, keys_set_1[i], light,style)
                elif i== 26 :
                    light = False
                    style = 0
                    letter(i, keys_set_1[i], light,style)

                else:
                    light = False
                    style = 1
                    letter(i, keys_set_1[i], light,style)
            cv2.putText(board,text,(10,100),font,3,0,2)

            #___________________________________________________________________________________________________
            # big_left_eye = cv2.resize(left_eye ,None, fx=5, fy=5)
            # big_th1 = cv2.resize(th1 ,None, fx=5, fy=5)
            # cv2.imshow("left_eye",big_left_eye)
            # cv2.imshow("thresholds_eye",big_th1)
            # cv2.imshow("Frame",frame)
            print('processed frame')
            print(frame.shape)
            mainLabel.setPixmap(frameToPixmap(frame))
            print('reach')
            #cv2.imshow("Virtual keyboard", keyboard )
            #cv2.imshow("Board",board)
            if exit_:
                break
    except BaseException as exc:
        print(exc)

pThread = Thread(target=mainProcessingLoop)
pThread.start()

def cleanup():
    print('cleanup')
    global exit_
    exit_ = True
    pThread.join()

    vs.stop()
    cv2.destroyAllWindows()

qapp.aboutToQuit.connect(cleanup)

qapp.exec_()
